import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      loading: false,
      pokemon: [],
      url: 'https://pokeapi.co/api/v2/pokemon',
      habilidades: [],
      elPokemon: "",
    }
    this.mostrarHabilidad = this.mostrarHabilidad.bind(this);
  }

  mostrarHabilidad(pokename){
    this.setState({
      loading:true,
      elPokemon: pokename
    });
    console.log(pokename);
    fetch(this.state.url + "/" + pokename)
    .then(res => res.json())
    .then (res => {
      this.setState({
        habilidades: res.abilities,
        loading: false
      })
    });
  }

  getPokemon = () => {
    this.setState({loading:true})
    fetch(this.state.url + "?limit=10")
    .then(res => res.json())
    .then (res => {
      
      this.setState({
        pokemon: res.results,
        loading: false
      })
    });
  };

  componentDidMount = () => {
    this.getPokemon();
  }

  render(){
    if(this.state.loading){
      return (
        <View style={styles.container}>
          <Text>Actualizando información</Text>
          <StatusBar style="auto" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.navbar}>
          <Text style={styles.title}>Pokemon</Text>
        </View>
        <FlatList data={this.state.pokemon} 
        renderItem={
          ({item}) => <Text onClick={() => {this.mostrarHabilidad(item.name)}} style={styles.item} >{item.name}</Text>
        }
        keyExtractor={(item, index) => index.toString() }
        />
        <Text style={styles.title}>Habilidades de {this.state.elPokemon}</Text>
        <FlatList data={this.state.habilidades} 
        renderItem={
          ({item}) => <Text>{item.ability.name}</Text>
        }
        keyExtractor={(item, index) => index.toString() }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    marginTop: 7,
    paddingVertical: 3,
    paddingHorizontal: 2,
    borderWidth: 1,
    borderColor: "#007bff",
    backgroundColor: "#fff",
    color: "#007bff",
    textAlign: "center",
    fontSize: 14,
  },
  navbar: {
    marginStart: 0,
    paddingHorizontal: 3,
    paddingVertical: 3,
    borderBottomWidth: 1,
    borderBottomColor: "#eaeaea",
  },
  title: {
    color: "#000",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
  }
});
